#!/usr/bin/bash

dotfilesRepo=""
branch=""

cd ${HOME}
sudo apt update && sudo apt upgrade -y
sudo apt install build-essential curl git cmake python3 pip libx11-dev libxtst-dev pkg-config -y

type -p curl >/dev/null || (sudo apt update && sudo apt install curl -y)
curl -fsSL https://cli.github.com/packages/githubcli-archive-keyring.gpg | sudo dd of=/usr/share/keyrings/githubcli-archive-keyring.gpg \
&& sudo chmod go+r /usr/share/keyrings/githubcli-archive-keyring.gpg \
&& echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/githubcli-archive-keyring.gpg] https://cli.github.com/packages stable main" | sudo tee /etc/apt/sources.list.d/github-cli.list > /dev/null \
&& sudo apt update \
&& sudo apt install gh -y
gh auth login

git clone --bare ${dotfilesRepo} ${HOME}/.gitdot \
&& git --git-dir=${HOME}/.gitdot --work-tree=${HOME} checkout -f ${branch} \
&& git --git-dir=${HOME}/.gitdot --work-tree=${HOME} config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*" \
&& git --git-dir=${HOME}/.gitdot --work-tree=${HOME} config status.showuntrackedfiles no

. .bashrc

cd ${HOME}
bash .otherdotfiles/bootstrap.sh
